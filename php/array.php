<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Document</title>
</head>
<body>
    <h1>Contoh soal array </h1>
    <?php
    echo "<h3>Contoh array ke 1 </h3>";
    $biodata = ["rizal", "nopal", "andhika"];
    print_r($biodata);

    echo "<h3> Contoh array 2 </h3>";
    echo "<h6> Total Traineer : ". count($biodata) . "</h6>";
    echo "<ol>";
    echo "<li>". $biodata[0] . "</li>";
    echo "<li>". $biodata[1] . "</li>";
    echo "<li>". $biodata[2] . "</li>";
    echo "</ol>";

    echo "<h3> Contoh array 3 </h3>";

    $biodataKedua = [
        ["nama" =>"rizky", "umur"=> 25, "kota"=> "Bekasi"],
        ["nama" =>"ganing", "umur"=> 28, "kota"=> "bandung"],
        ["nama" =>"puput", "umur"=> 19, "kota"=> "magelang"]
        

    ];
    print_r($biodataKedua);
    

?>
</body>
</html>